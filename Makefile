docker_exec_php = docker compose exec php
docker_exec_php_console = docker compose exec php bin/console

up: ## Start containers
	docker compose up

ssh: ## Open the php docker container terminal
	$(docker_exec_php) /bin/sh

help:
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

.DEFAULT_GOAL := help