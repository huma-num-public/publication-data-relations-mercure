<?php

namespace App\Model;

class MercureDataRelation
{
    public string $iri;
    public string $relation;
    public string $action;
    public ?string $comment = null;

    public string $repository;
}