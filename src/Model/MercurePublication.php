<?php

namespace App\Model;

class MercurePublication
{
    public string $topic;
    public string $iri;

    /**
     * @var array|MercureDataRelation[]
     */
    public array $relations;
}