<?php

namespace App\Command;

use App\Model\MercurePublication;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\DependencyInjection\ParameterBag\ContainerBagInterface;
use Symfony\Component\HttpClient\Chunk\ServerSentEvent;
use Symfony\Component\HttpClient\EventSourceHttpClient;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\Mercure\HubInterface;

#[AsCommand(
    name: 'daemon:run',
    description: 'Run mercure daemon listening to an event',
)]
class DaemonRunCommand extends Command
{
    private string $respository;

    public function __construct(
        private HubInterface $subscriber,
        private ContainerBagInterface $containerBag,
    ) {
        parent::__construct();

        $this->respository = $containerBag->get('targetRepository');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        /**
         * You may want to listen to your repository events
         */
        $url = $this->subscriber->getUrl().'?topic='.$this->respository;
        $io->block('Listening to '.$url);

        $options = [
            'auth_bearer' => $this->subscriber->getFactory()->create([$this->respository]),
        ];

        $client = HttpClient::create($options);
        $client = new EventSourceHttpClient($client, 10);
        $source = $client->connect($url);

        while ($source) {
            foreach ($client->stream($source, 10) as $r => $chunk) {
                if ($chunk->isTimeout()) {
                    /**
                     * You can log timeouts if needed
                     */
                    continue;
                }

                /**
                 * Each time mercure is hit by an event concerning your repository, this code will be executed
                 */
                if ($chunk instanceof ServerSentEvent) {
                    /**
                     * @var MercurePublication $response
                     */
                    $response = json_decode($chunk->getData());


                    /**
                     * The response contains one publication, containing one or several relations
                     */
                    foreach ($response->relations as $relation) {

                        /**
                         * ...code your own logic
                         *
                         * You may want to persist relations in your database after doing some checking :
                         * - do the subject exist in my database?
                         * - do the relation exists in Datacite Reference?
                         * - do the relation already exists?
                         * - ...
                         *
                         */

                        $io->success(
                            sprintf(
                                'Nakala data %s %s relation %s %s with comment : "%s"',
                                $response->iri,
                                $relation->action,
                                $relation->relation,
                                $relation->iri,
                                $relation->comment,
                            )
                        );
                    }
                }
            }
        }

        return Command::SUCCESS;
    }
}
