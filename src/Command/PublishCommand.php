<?php

namespace App\Command;

use App\Model\MercureDataRelation;
use App\Model\MercurePublication;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\DependencyInjection\ParameterBag\ContainerBagInterface;
use Symfony\Component\Mercure\HubInterface;
use Symfony\Component\Mercure\Update;

#[AsCommand(
    name: 'app:publish',
    description: 'Publish a relation to a repository using mercure',
)]
class PublishCommand extends Command
{
    private string $respository;

    public function __construct(
        private HubInterface $publisher,
        private ContainerBagInterface $containerBag,
    ) {
        parent::__construct();

        $this->respository = $containerBag->get('targetRepository');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        /**
         * Multiples relations can be added when relations concern the same IRI and target repository (the topic here)
         *
         * In this example, the user specified two relations :
         * 1. he added IsReferencedBy towards hal-03110771v1
         * 2. he removed Cites towards hal-03442576v1
         */

        //Relation 1
        $relation = new MercureDataRelation();
        $relation->iri = 'hal-03110771v1';
        $relation->relation = 'IsReferencedBy';
        $relation->repository = 'hal';
        $relation->action = 'add';
        $relation->comment = 'A comment about the relation';
        $relations[] = $relation;

        //Relation 2
        $relation = new MercureDataRelation();
        $relation->iri = 'hal-03442576v1';
        $relation->relation = 'Cites';
        $relation->repository = 'hal';
        $relation->action = 'remove';
        $relations[] = $relation;

        $publication = new MercurePublication();
        $publication->iri = "http://nakala.local/10.34847/nkl.28227db0";
        $publication->topic = $this->respository;
        $publication->relations = $relations;

        $io->block(sprintf('Will try to reach and send data to Mercure server at %s', $this->publisher->getUrl()));

        /**
         * Objects are json-encoded and then sent via a new Update(),
         * then the Update is passed to a publish() method
         */
        $update = new Update(
            $publication->topic,
            json_encode($publication),
            true
        );

        try {
            $this->publisher->publish($update);
        } catch (\Exception $exception) {
            $io->error($exception->getMessage());

            return Command::FAILURE;
        }

        $io->success('Data have been published, see your daemon logs!');

        return Command::SUCCESS;
    }
}
