# Mercure listener demo project

This project is based on Symfony 5.3 and PHP 8.

It's a proof of concept and can be a good start to implements :
- subscribing to your repository events (the relations) submitted to a mercure server,
- pushing your own events to a mercure server.

You are free to reuse or modify this project.

# Project files

- `config/packages/mercure.yaml` : configuration for the mercure bundle
- `src/Models` : contains the publication models
- `src/Command` : contains both publish/listen commands

# Run the project

## Prerequisite

Docker have to be available on your system.

You can connect to eh php container with `make ssh`

## Configuration

### Use project provided server

The `.env` file contain configuration for the mercure server provided in [docker-compose.yml](./docker-compose.yml)

### Use Nakala mercure dev server

If you want to use the Nakala mercure dev server, create a `.env.local` file containing following configuration data :

```
MERCURE_URL=http://127.0.0.1:82/.well-known/mercure
MERCURE_PUBLIC_URL=http://127.0.0.1:82/.well-known/mercure
MERCURE_JWT_PUBLISHER="!PublisherKey!"
MERCURE_JWT_SUBSCRIBER="!SubscriberKey!"
```

### Configuration data details

- `MERCURE_URL` : the internal url of mercure server (different from public if you use a docker container)
- `MERCURE_PUBLIC_URL` : the external url of mercure server
- `MERCURE_JWT_PUBLISHER` : the secret passphrase key to be able to publish to mercure server
- `MERCURE_JWT_SUBSCRIBER` : the secret passphrase key to be able to subscribe to mercure server
- `TARGET_REPOSITORY` : the target repository you want to publish or subscribe for

## Listen to published events

Command is here : [DaemonRunCommand.php](src/Command/DaemonRunCommand.php)

Start container
```
docker compose up

# via make
make up
```

Install dependencies
```
# in your host
docker compose exec php composer install

# in docker
composer install
```

Launch daemon
```
# in your host
docker compose exec php php bin/console daemon:run

# in docker
php bin/console daemon:run
```

Reach mercure dev UI [http://localhost:8001/.well-known/mercure/ui/](http://localhost:8001/.well-known/mercure/ui/).

In the publish part, topics field, enter `nakala`

In the data field, enter : 

```json
{
  "topic": "nakala",
  "iri": "http:\/\/nakala.local\/10.34847\/nkl.28227db0",
  "relations": [
    {
      "iri": "hal-03110771v1",
      "relation": "IsReferencedBy",
      "action": "remove",
      "comment": "A comment about the relation",
      "repository": "hal"
    },
    {
      "iri": "hal-03442576v1",
      "relation": "HasPart",
      "action": "remove",
      "comment": null,
      "repository": "hal"
    }
  ]
}
```

Dameon must have responded to your publication by displaying :
```
[OK] Nakala data http://nakala.local/10.34847/nkl.28227db0 remove relation IsReferencedBy hal-03110771v1 with
     comment : "A "right" comment about the relation"
     
[OK] Nakala data http://nakala.local/10.34847/nkl.28227db0 remove relation HasPart hal-03442576v1 with comment : ""
```

## Publish events

Command is here : [PublishCommand.php](src/Command/PublishCommand.php)

Publish an event from 
```
# from docker
php bin/console app:publish
```

Dameon must have responded to your publication by displaying :
```
 [OK] Nakala data http://nakala.local/10.34847/nkl.28227db0 add relation IsReferencedBy hal-03110771v1 with comment :
      "A comment about the relation"
     
 [OK] Nakala data http://nakala.local/10.34847/nkl.28227db0 remove relation Cites hal-03442576v1 with comment : ""
```

## Publication models

You can find the publication models here :

- [MercurePublication](src/Model/MercurePublication.php)
- [MercureDataRelation](src/Model/MercureDataRelation.php)

Here is a description of a publication on Mercure :

- `topic` : the channel where the message is published and intended for the subscriber
- `iri` : the URI of the subject of the relation
- `relations` : the list of relations to add or delete
  - `iri` : the identifier of the object of the relation
  - `relation` : the name of the relation
  - `action` : the action that the subscriber has to do with this relation
  - `comment` : a comment about the relation
  - `repository` : the repository of the object of the relation

## Use cases

If a user from NAKALA repository adds the relation `10.34847/nkl.62561d13 IsCitedBy hal-01848367v1`, NAKALA repository has to publish the inverted relation on Mercure with the following content for HAL repository :

```json
{
   "topic": "hal",
   "iri": "https:\/\/hal.halpreprod.archives-ouvertes.fr\/hal-01848367v1",
   "relations": [
      {
         "iri":"10.34847\/nkl.62561d13",
         "relation":"Cites",
         "action":"add",
         "comment":null,
         "repository":"nakala"
      }
   ]
}
```

If a user from HAL repository adds the relation `hal-01848367v1 Cites 10.34847/nkl.62561d13`, HAL repository has to publish the inverted relation on Mercure with the following content for NAKALA repository :

```json
{
   "topic":"nakala",
   "iri":"https:\/\/test.nakala.fr\/10.34847\/nkl.62561d13",
   "relations": [
      {
         "iri":"hal-01848367v1",
         "relation":"IsCitedBy",
         "action":"add",
         "comment":null,
         "repository":"hal"
      }
   ]
}
```